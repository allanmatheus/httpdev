from os import environ
from pprint import pprint
from http.server import HTTPStatus, HTTPServer, BaseHTTPRequestHandler


class PrintRequestHandler(BaseHTTPRequestHandler):

    def print_request(self):
        headers = dict(self.headers)
        nbytes = int(headers.get('Content-Length', '0'))
        raw_data = self.rfile.read(nbytes).strip().decode()
        pprint(dict(command=self.command, path=self.path, headers=headers,
                    version=self.request_version, data=raw_data))

    def do_ANY(self):
        self.print_request()
        self.send_response(HTTPStatus.OK)
        self.send_header('Content-Type', 'application/text')
        self.end_headers()

    def do_HEAD(self):
        self.do_ANY()

    def do_GET(self):
        self.do_ANY()

    def do_POST(self):
        self.do_ANY()

    def do_PUT(self):
        self.do_ANY()

    def do_DELETE(self):
        self.do_ANY()


def run(server_cls, handler_cls):
    host = environ.get('HOST', '0.0.0.0')
    port = int(environ.get('PORT', '8000'))

    server_address = (host, port)
    httpd = server_cls(server_address, handler_cls)

    print(f'Listening on http://{host}:{port}/')

    httpd.serve_forever()

if __name__ == '__main__':
    run(HTTPServer, PrintRequestHandler)
