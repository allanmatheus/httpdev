FROM python:3.7
COPY httpdev.py /app/httpdev.py
EXPOSE 8000
CMD ["python", "/app/httpdev.py"]
